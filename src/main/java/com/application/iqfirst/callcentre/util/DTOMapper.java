package com.application.iqfirst.callcentre.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.application.iqfirst.callcentre.dto.CaseDetailsDTO;
import com.application.iqfirst.callcentre.dto.CasePartnerMappingDTO;
import com.application.iqfirst.callcentre.dto.PartnerDetailDTO;
import com.application.iqfirst.callcentre.pojo.CaseDetails;
import com.application.iqfirst.callcentre.pojo.CasePartnerMapping;
import com.application.iqfirst.callcentre.pojo.CaseStatusDescription;
import com.application.iqfirst.callcentre.pojo.GeoLocation;
import com.application.iqfirst.callcentre.pojo.PartnerDetail;
import com.application.iqfirst.callcentre.pojo.TestPackage;
import com.application.iqfirst.callcentre.repository.CaseStatusDescriptionRepository;
import com.application.iqfirst.callcentre.repository.IQFirstGeoLocationRepository;
import com.application.iqfirst.callcentre.repository.IQFirstInsuranceCompanyRepository;
import com.application.iqfirst.callcentre.repository.IQFirstPackageRepository;
import com.application.iqfirst.callcentre.repository.IQFirstPartnerDetailRepository;

@Service
public class DTOMapper {
	
	@Autowired
	IQFirstGeoLocationRepository 				iQGeoLocationRepository;
	
	@Autowired
	IQFirstInsuranceCompanyRepository 			iQFirstInsuranceCompanyRepository;
	
	@Autowired
	IQFirstPackageRepository 					iQFirstPackageRepository;
	
	@Autowired
	IQFirstPartnerDetailRepository 				iQFirstPartnerDetailRepository;
	
	@Autowired
	CaseStatusDescriptionRepository             caseStatusDescriptionRepository;
	
	@Autowired
	CallCentreUtil callCentreUtil;
	
	
	public CaseDetailsDTO caseDetailstoDTO(CaseDetails caseDetails){
		
		GeoLocation geoLocation = null;
		CaseDetailsDTO caseDetailsDTO = new CaseDetailsDTO();
		try {
			Set<String> testPackages = new HashSet<>();
			caseDetailsDTO = new CaseDetailsDTO();
			
			caseDetailsDTO.setCaseId(String.valueOf(caseDetails.getCaseId()));
			caseDetailsDTO.setInsuranceCompany(caseDetails.getInsuranceCompany().getCompName());
			caseDetailsDTO.setPolicyNo(caseDetails.getPolicyNo());
			caseDetailsDTO.setCustomerName(caseDetails.getCustomerName());
			caseDetailsDTO.setAddress(caseDetails.getAddress());
			caseDetailsDTO.setPin(caseDetails.getPin());
			caseDetailsDTO.setLatLng(caseDetails.getLatLng());
			caseDetailsDTO.setRegDt(caseDetails.getRegDt());
			caseDetailsDTO.setCustomerCO(caseDetails.getCustomerCO());
			caseDetailsDTO.setCustomerCode(caseDetails.getCustomerCode());
			
			geoLocation = iQGeoLocationRepository.findLocationById(caseDetails.getCityId());
			if(geoLocation!=null) {
				caseDetailsDTO.setCity(geoLocation.getName());
				
				if(geoLocation.getParent() != null){
					caseDetailsDTO.setDist(geoLocation.getParent().getName());
					caseDetailsDTO.setDistId(geoLocation.getParent().getSdcId());
				}
				if(geoLocation.getParent().getParent() != null){
					caseDetailsDTO.setState(geoLocation.getParent().getParent().getName());
					caseDetailsDTO.setStateId(geoLocation.getParent().getParent().getSdcId());
				}
				caseDetailsDTO.setCityId(geoLocation.getSdcId());
				caseDetailsDTO.setDistId(geoLocation.getParent().getSdcId());
			}
			caseDetailsDTO.setMobileNo1(caseDetails.getMobileNo1());
			caseDetailsDTO.setMobileNo2(caseDetails.getMobileNo2());
			caseDetailsDTO.setSa(caseDetails.getSa());
			caseDetailsDTO.setCaseType(caseDetails.getCaseType());
			caseDetailsDTO.setPaymentBy(caseDetails.getPaymentBy());
			CaseStatusDescription csd = caseStatusDescriptionRepository.findByStatusId(caseDetails.getCaseStatus());
			caseDetailsDTO.setCaseStatus(csd.getStatusName());
			caseDetailsDTO.setCreatedBy(caseDetails.getCreatedBy());
			if(caseDetails.getCreateddate() != null)
				caseDetailsDTO.setCreateddate(caseDetails.getCreateddate().toString());
			caseDetailsDTO.setUpdatedby(caseDetails.getUpdatedby());
			if(caseDetails.getUpdateddate() != null)
				caseDetailsDTO.setUpdateddate(caseDetails.getUpdateddate().toString());
			for(TestPackage tp : caseDetails.getTestPackage()){
				testPackages.add(String.valueOf(tp.getPackageTests()));
			}
			caseDetailsDTO.setPackages(testPackages);
		}catch(Exception e) {}
		
		return caseDetailsDTO;
	}
	
	public PartnerDetailDTO partnersDetailsToDTO(PartnerDetail partnerDetail){
		PartnerDetailDTO partnerDetailDTO = new PartnerDetailDTO();
		GeoLocation geoLocation = null;
		if(partnerDetail.getCityId() != null){
			geoLocation = iQGeoLocationRepository.findLocationById(partnerDetail.getCityId());
			partnerDetailDTO.setCity(geoLocation.getName());
		}
		if(partnerDetail.getDistId() != null){
			geoLocation = iQGeoLocationRepository.findLocationById(partnerDetail.getDistId());
			partnerDetailDTO.setDistrict(geoLocation.getName());
		}
		if(partnerDetail.getStateId() != null){
			geoLocation = iQGeoLocationRepository.findLocationById(partnerDetail.getStateId());
			partnerDetailDTO.setState(geoLocation.getName());
		}
		BeanUtils.copyProperties(partnerDetail, partnerDetailDTO);
		return partnerDetailDTO;
	}
	
	@Transactional
	public CasePartnerMapping dtoTocasePartnerMapping (CasePartnerMappingDTO casePartnerMappingDTO) throws ParseException{
		CasePartnerMapping casePartnerMapping = new CasePartnerMapping();
		BeanUtils.copyProperties(casePartnerMappingDTO, casePartnerMapping);
		casePartnerMapping.setScheduledDate(Timestamp.valueOf(casePartnerMappingDTO.getScheduledDate()));
		if(casePartnerMappingDTO.getIsHomeVisit() != null){
			casePartnerMapping.setIsHomeVisit(casePartnerMappingDTO.getIsHomeVisit());
		}else{
			casePartnerMapping.setIsHomeVisit('N');
		}
		return casePartnerMapping;
	}
	
	@Transactional
	public CasePartnerMappingDTO casePartnerToDTO(CasePartnerMapping casePartnerMapping){
		CasePartnerMappingDTO casePartnerMappingDTO = new CasePartnerMappingDTO();
		BeanUtils.copyProperties(casePartnerMapping, casePartnerMappingDTO);
		casePartnerMappingDTO.setScheduledDate(casePartnerMapping.getScheduledDate().toString());
		
		return casePartnerMappingDTO;
	}
	
	public CasePartnerMappingDTO casePartnerToDTO(CasePartnerMapping casePartnerMapping, CasePartnerMappingDTO casePartnerMappingDTO){
		BeanUtils.copyProperties(casePartnerMapping, casePartnerMappingDTO);
		return casePartnerMappingDTO;
	}
	
}
