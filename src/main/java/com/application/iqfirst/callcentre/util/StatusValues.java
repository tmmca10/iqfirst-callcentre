package com.application.iqfirst.callcentre.util;

public class StatusValues {
	private StatusValues() { }
	public static final Integer UNASSIGNED 		= 1;
	public static final Integer ASSIGNED 		= 2;
	public static final Integer SAMPLECOLLECTED = 3;
	public static final Integer REPORTUPLOADED 	= 4;
	public static final Integer QCDONE 			= 5;
	public static final Integer CLOSED 			= 6;
	public static final Integer ALLOCATED 		= 7;

}
