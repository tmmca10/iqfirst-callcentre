package com.application.iqfirst.callcentre.util;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.application.iqfirst.callcentre.dto.CasePartnerMappingDTO;
import com.application.iqfirst.callcentre.pojo.CaseDetails;
import com.application.iqfirst.callcentre.pojo.CaseHistory;
import com.application.iqfirst.callcentre.pojo.CasePkgMapping;
import com.application.iqfirst.callcentre.pojo.CaseStatusDescription;
import com.application.iqfirst.callcentre.pojo.GeoLocation;
import com.application.iqfirst.callcentre.pojo.InsuranceCompany;
import com.application.iqfirst.callcentre.pojo.PartnerDetail;
import com.application.iqfirst.callcentre.pojo.TestPackage;
import com.application.iqfirst.callcentre.repository.CaseHistoryRepository;
import com.application.iqfirst.callcentre.repository.CasePackageMappingRepository;
import com.application.iqfirst.callcentre.repository.CaseStatusDescriptionRepository;
import com.application.iqfirst.callcentre.repository.IQFirstCaseDetailsRepository;
import com.application.iqfirst.callcentre.repository.IQFirstCaseStatusHistoryRepository;
import com.application.iqfirst.callcentre.repository.IQFirstGeoLocationRepository;
import com.application.iqfirst.callcentre.repository.IQFirstInsuranceCompanyRepository;
import com.application.iqfirst.callcentre.repository.IQFirstPartnerDetailRepository;
import com.application.iqfirst.callcentre.repository.TestPackageRepository;

@Service
public class CallCentreUtil {
	private static final Logger log = LoggerFactory.getLogger(CallCentreUtil.class);
	
	@Autowired
	IQFirstGeoLocationRepository 		iQGeoLocationRepository;
	@Autowired
	IQFirstCaseDetailsRepository 		iQFirstCaseDetailsRepository;
	@Autowired
	IQFirstCaseStatusHistoryRepository 	iQFirstCaseStatusHistoryRepository;
	@Autowired
	IQFirstPartnerDetailRepository 		iQFirstPartnerDetailRepository;
	@Autowired
	CaseHistoryRepository               caseHistoryRepository;
	@Autowired
	CaseStatusDescriptionRepository     caseStatusDescriptionRepository;
	@Autowired
	CasePackageMappingRepository		casePackageMappingRepository;
	@Autowired
	IQFirstInsuranceCompanyRepository	iQFirstInsuranceCompanyRepository;
	@Autowired
	TestPackageRepository				testPackageRepository;
	
	public CaseDetails updateCaseDetails(Long caseDetailsId, String userName){
		CaseDetails caseDetails = iQFirstCaseDetailsRepository.findByID(caseDetailsId);
		caseDetails.setCaseStatus(StatusValues.ASSIGNED);
		caseDetails.setUpdatedby(userName);
		caseDetails.setUpdateddate(new Timestamp(System.currentTimeMillis()));
		try{
			iQFirstCaseDetailsRepository.save(caseDetails);
		}catch (Exception e) {
			log.error("Problem encouner in CallCentreUtil class at updateCaseDetails method");// TODO: handle exception
		}
		return caseDetails;
	}
	
	public void updateCaseHistory(int status, Long caseId, String description,String userName){
		try{
			CaseStatusDescription csd = caseStatusDescriptionRepository.findByStatusId(status);
			CaseHistory caseHistory = new CaseHistory(csd.getStatusId(), caseId, description, new Timestamp(System.currentTimeMillis()), userName);
			caseHistoryRepository.save(caseHistory);
		}catch (Exception e) {
			log.error("Problem encounter in CallCentreUtil class at updateCaseHistory method");
		}
	}
	
	public void setLocations(CaseDetails caseDetails, CasePartnerMappingDTO casePartnerMappingDTO){
		try{
			GeoLocation geoLocation = iQGeoLocationRepository.findLocationById(caseDetails.getCityId());
			if(caseDetails.getCityId() != null)
				geoLocation = iQGeoLocationRepository.findLocationById(caseDetails.getCityId());
			
			casePartnerMappingDTO.setCustomerCity(geoLocation.getName());
			
			if(geoLocation.getParent() != null)
				casePartnerMappingDTO.setCustomerDistrict(geoLocation.getParent().getName());
			
			if(geoLocation.getParent().getParent() != null)
				casePartnerMappingDTO.setCustomerState(geoLocation.getParent().getParent().getName());
		}catch (Exception e) {
			log.error("Problem encountered in CallCentreUtil class at setLocations method. Error: "+e.getMessage());
		}
	}
	
	@Transactional
	public CasePartnerMappingDTO processCasePartnerMappingDTO(String userName, CasePartnerMappingDTO casePartnerMappingDTO, String status){
		Set<String> packages = new HashSet<>();
		PartnerDetail partnerDetail = iQFirstPartnerDetailRepository.findByPartnerId(casePartnerMappingDTO.getPartnerId());
		String providerName = partnerDetail.getProviderName();
		String providercode = partnerDetail.getProviderCode();
		if(status.equalsIgnoreCase("update")){
			CaseDetails caseDetails = updateCaseDetails(casePartnerMappingDTO.getCasedetailsId(), userName);
			caseDetails.getTestPackage().stream().forEach(testPackages -> packages.add(testPackages.getPackageCd()));
			casePartnerMappingDTO.setProviderName(providerName);
			casePartnerMappingDTO.setCustomerName(caseDetails.getCustomerName());
			casePartnerMappingDTO.setCustomerMobileNo(caseDetails.getMobileNo1());
			casePartnerMappingDTO.setPackages(packages);
			casePartnerMappingDTO.setPolicyNumber(caseDetails.getPolicyNo());
			casePartnerMappingDTO.setCustomerCode(caseDetails.getCustomerCode());
			casePartnerMappingDTO.setProviderCode(providercode);
			if(casePartnerMappingDTO.getIsHomeVisit() == null)
				casePartnerMappingDTO.setIsHomeVisit('N');
			if(casePartnerMappingDTO.getIsHomeVisit().equals('Y') || casePartnerMappingDTO.getIsHomeVisit().equals('y')){
				mapHomeVisitPackage(caseDetails);
			}
			setLocations(caseDetails, casePartnerMappingDTO);
			updateCaseHistory(caseDetails.getCaseStatus(), caseDetails.getCaseId(), "The Process has been updated", userName);
		}else if(status.equalsIgnoreCase("create")){
			CaseDetails caseDetails = updateCaseDetails(casePartnerMappingDTO.getCasedetailsId(), userName);
			caseDetails.getTestPackage().stream().forEach(testPackages -> packages.add(testPackages.getPackageCd()));
			casePartnerMappingDTO.setCreatedBy(userName);
			casePartnerMappingDTO.setCreatedDate(new Timestamp(System.currentTimeMillis()));
			if(casePartnerMappingDTO.getIsHomeVisit() == null)
				casePartnerMappingDTO.setIsHomeVisit('N');
			if(casePartnerMappingDTO.getIsHomeVisit().equals('Y') || casePartnerMappingDTO.getIsHomeVisit().equals('y')){
				mapHomeVisitPackage(caseDetails);
			}
			casePartnerMappingDTO.setProviderName(providerName);
			casePartnerMappingDTO.setCustomerName(caseDetails.getCustomerName());
			casePartnerMappingDTO.setCustomerMobileNo(caseDetails.getMobileNo1());
			casePartnerMappingDTO.setPackages(packages);
			casePartnerMappingDTO.setPolicyNumber(caseDetails.getPolicyNo());
			casePartnerMappingDTO.setCustomerCode(caseDetails.getCustomerCode());
			casePartnerMappingDTO.setProviderCode(providercode);
			setLocations(caseDetails, casePartnerMappingDTO);
			updateCaseHistory(caseDetails.getCaseStatus(), caseDetails.getCaseId(), "Case is now assigned to: "+casePartnerMappingDTO.getProviderName(), userName);
		}else{
			CaseDetails caseDetails = iQFirstCaseDetailsRepository.findByID(casePartnerMappingDTO.getCasedetailsId());
			casePartnerMappingDTO.setProviderName(providerName);
			casePartnerMappingDTO.setProviderCode(providercode);
			casePartnerMappingDTO.setCustomerName(caseDetails.getCustomerName());
			casePartnerMappingDTO.setCustomerMobileNo(caseDetails.getMobileNo1());
			caseDetails.getTestPackage().stream().forEach(testPackages -> packages.add(testPackages.getPackageCd()));
			casePartnerMappingDTO.setPackages(packages);
			casePartnerMappingDTO.setPolicyNumber(caseDetails.getPolicyNo());
			casePartnerMappingDTO.setCustomerCode(caseDetails.getCustomerCode());
			setLocations(caseDetails, casePartnerMappingDTO);
		}
		return casePartnerMappingDTO;
	}
	
	public void mapHomeVisitPackage(CaseDetails caseDetails){
		InsuranceCompany 	ic 	= iQFirstInsuranceCompanyRepository.findByComId(caseDetails.getInsuranceCompany().getCompId());
		List<TestPackage>	tps = testPackageRepository.findByComIdAndPackage(ic.getCompId(), "HOMEVISIT");	
		if(tps != null && !tps.isEmpty()){
			CasePkgMapping cpm = new CasePkgMapping(caseDetails.getCaseId(), tps.get(0).getPackageId());
			try{
				casePackageMappingRepository.save(cpm);
				log.info("Home Visit package added for case '"+caseDetails.getCustomerCode()+"'");
			}catch (Exception e) {
				log.error("There is a problem to save home visit package. Error: "+e.getMessage());
			}
		}else{
			log.info("Home Visit package not available");
		}
	}
	
	public ResponseEntity<String> sendEmail(CasePartnerMappingDTO cpm, String mailUrl) {
		JSONObject userObj = new JSONObject();
		PartnerDetail partner = iQFirstPartnerDetailRepository.findByPartnerId(cpm.getPartnerId());
		try {
			userObj.put("toMail", partner.getsPOCmailID());
			userObj.put("subject", "New case assigned: "+cpm.getCustomerCode());
			userObj.put("body", "Dear Sir, <br> One case has been assigned to your Diagnostic Centre : "
					+ ""+ cpm.getCustomerName()+"( "+cpm.getCustomerCode()+" )"+ 
					"<br> Policy number: "+cpm.getPolicyNumber()+
					"<br> Appointment scheduled on "+cpm.getScheduledDate()+"<br> Packages: "+cpm.getPackages()
					+ "<br> Thanks & Regards,<br> IQ First Health Care Pvt Ltd.");
		} catch (JSONException e) {
			log.error("Problem in CallCentreUtil class at sendEmail method. Error: "+e.getMessage());
		}
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(userObj.toString(), headers);
		RestTemplate template = new RestTemplate();
		ResponseEntity<String> response = template.postForEntity(mailUrl, request,
				String.class);
 		return new ResponseEntity<>(response.getBody(),response.getStatusCode());
	}


}
