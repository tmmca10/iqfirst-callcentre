package com.application.iqfirst.callcentre.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.application.iqfirst.callcentre.pojo.CaseDetails;
import com.application.iqfirst.callcentre.pojo.CaseStatusDescription;
import com.application.iqfirst.callcentre.pojo.InsuranceCompany;
import com.application.iqfirst.callcentre.pojo.PartnerDetail;
import com.application.iqfirst.callcentre.repository.CaseStatusDescriptionRepository;
import com.application.iqfirst.callcentre.repository.IQFirstCaseDetailsRepository;
import com.application.iqfirst.callcentre.repository.IQFirstInsuranceCompanyRepository;
import com.application.iqfirst.callcentre.repository.IQFirstPartnerDetailRepository;

@Service
public class QueryBuilder {
	private static final Logger log = LoggerFactory.getLogger(QueryBuilder.class);
	
	@Autowired
	IQFirstPartnerDetailRepository 			iQFirstPartnerDetailRepository;
	
	@Autowired
	IQFirstCaseDetailsRepository 			iQFirstCaseDetailsRepository;
	
	@Autowired
	IQFirstInsuranceCompanyRepository		iQFirstInsuranceCompanyRepository;
	
	@Autowired
	CaseStatusDescriptionRepository     	caseStatusDescriptionRepository;
	
	@SuppressWarnings("serial")
	public List<PartnerDetail> findPartnersByCriteria(Map<String, String> map) {
		try{
			return iQFirstPartnerDetailRepository.findAll(new Specification<PartnerDetail>() {

				@Override
				public Predicate toPredicate(Root<PartnerDetail> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					if(map != null){
						map.entrySet().stream()
				        .forEach(
				                pair -> {
				                    if (pair.getKey().equalsIgnoreCase("preferredProviderPartner")) {
				                    	predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get(pair.getKey()), pair.getValue().charAt(0))));
				                    }else if(pair.getKey().equalsIgnoreCase("pincode")){
				                    	predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("Pincode"), pair.getValue())));
				                    }else if(pair.getKey().equalsIgnoreCase("isActive")){
				                    	predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("isActive"), pair.getValue().charAt(0))));
				                    }else{
				                    	predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get(pair.getKey()), pair.getValue())));
				                    }
				                }
				        );
					}else{
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("isActive"), 'Y')));
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("isActive"), 'y')));
					}
	                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}});
		}catch (Exception e) {
			log.error("Problem encounter while generate Query at findPartnersByCriteria method. Error: "+e.getMessage());
			return new ArrayList<>();
		}
		
	}
	
	@SuppressWarnings("serial")
	public List<CaseDetails> getAllCaseDetailsByCriteria(Map<String, String> map) {
		try{
			return iQFirstCaseDetailsRepository.findAll(new Specification<CaseDetails>() {

				@Override
				public Predicate toPredicate(Root<CaseDetails> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<>();
					if(map != null){
						map.entrySet().stream()
				        .forEach(
				                pair -> {
				                    if (pair.getKey().equalsIgnoreCase("insuranceCompany")) {
				                    	InsuranceCompany inc = iQFirstInsuranceCompanyRepository.findByName(pair.getValue());
				                    	predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("InsuranceCompany"), inc.getCompId())));
				                    }else if(pair.getKey().equalsIgnoreCase("caseStatus")){
				                    	CaseStatusDescription csd = caseStatusDescriptionRepository.findByName(pair.getValue().trim().toUpperCase());
				                    	predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get(pair.getKey()), csd.getStatusId())));
				                    }else{
				                    	predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get(pair.getKey()), pair.getValue())));
				                    }
				                }
				        );
					}else
						predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("caseStatus"), StatusValues.UNASSIGNED)));

					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}});
		}catch (Exception e) {
			log.error("Problem encountered in QueryBuilder class at getAllCaseDetailsByCriteria method. Error: "+e.getMessage());
			return new ArrayList<>();
		}
	}

}
