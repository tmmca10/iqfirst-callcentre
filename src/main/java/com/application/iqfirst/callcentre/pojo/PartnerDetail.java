package com.application.iqfirst.callcentre.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "partnerDetail")
public class PartnerDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long 		partnerId;
	
	private String 		providerCode;
	private String 		grades;
	private String 		pANNo;
	private String 		providerName;
	private String 		address1;
	private String 		address2;
	private String 		address3;
	private String 		location;
	private Long 		cityId;
	private Long 		distId;
	private Long 		stateId;
	private String 		Pincode;
	private String 		stdCode;
	private String 		landlineNo;
	private String 		mobileNo;
	private String 		dCLocation;
	private Character 	homevisit;
	private Character 	preferredProviderPartner;
	private Character 	isActive;
	private String 		empanelledSince;
	private String 		sPOCName;
	private String 		sPOCmobile;
	private String 		sPOCmailID;
	private String 		tPAManagerName;
	private String 		tPAMMobile;

	@Column(name = "TPAMEMail")
	private String tPAMEMail;
	private String marketingHead;
	private String mHMobile;
	private String mHMail;
	private String ownerName;
	private String ownerMobile;
	private String ownermail;
	private String bankACNo;
	private String bankName;
	private String branchName;
	private String ifscCode;

	private Character mer;
	private Character ecg;
	private Character pathology;
	private Character chestXRay;
	private Character tmt;
	private Character echo;
	private Character pft;
	private Character usg;
	private Character urineNicotine;


	public Character getMer() {
		return mer;
	}

	public void setMer(Character mer) {
		this.mer = mer;
	}

	public Character getEcg() {
		return ecg;
	}

	public void setEcg(Character ecg) {
		this.ecg = ecg;
	}

	public Character getPathology() {
		return pathology;
	}

	public void setPathology(Character pathology) {
		this.pathology = pathology;
	}

	public Character getChestXRay() {
		return chestXRay;
	}

	public void setChestXRay(Character chestXRay) {
		this.chestXRay = chestXRay;
	}

	public Character getTmt() {
		return tmt;
	}

	public void setTmt(Character tmt) {
		this.tmt = tmt;
	}

	public Character getEcho() {
		return echo;
	}

	public void setEcho(Character echo) {
		this.echo = echo;
	}

	public Character getPft() {
		return pft;
	}

	public void setPft(Character pft) {
		this.pft = pft;
	}

	public Character getUsg() {
		return usg;
	}

	public void setUsg(Character usg) {
		this.usg = usg;
	}

	public Character getUrineNicotine() {
		return urineNicotine;
	}

	public void setUrineNicotine(Character urineNicotine) {
		this.urineNicotine = urineNicotine;
	}
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getBankACNo() {
		return bankACNo;
	}

	public void setBankACNo(String bankACNo) {
		this.bankACNo = bankACNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getGrades() {
		return grades;
	}

	public void setGrades(String grades) {
		this.grades = grades;
	}

	public String getpANNo() {
		return pANNo;
	}

	public void setpANNo(String pANNo) {
		this.pANNo = pANNo;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getDistId() {
		return distId;
	}

	public void setDistId(Long distId) {
		this.distId = distId;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public String getPincode() {
		return Pincode;
	}

	public void setPincode(String Pincode) {
		this.Pincode = Pincode;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getLandlineNo() {
		return landlineNo;
	}

	public void setLandlineNo(String landlineNo) {
		this.landlineNo = landlineNo;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getdCLocation() {
		return dCLocation;
	}

	public void setdCLocation(String dCLocation) {
		this.dCLocation = dCLocation;
	}

	public Character getHomevisit() {
		return homevisit;
	}

	public void setHomevisit(Character homevisit) {
		this.homevisit = homevisit;
	}

	public Character getPreferredProviderPartner() {
		return preferredProviderPartner;
	}

	public void setPreferredProviderPartner(Character preferredProviderPartner) {
		this.preferredProviderPartner = preferredProviderPartner;
	}

	public Character getIsActive() {
		return isActive;
	}

	public void setIsActive(Character isActive) {
		this.isActive = isActive;
	}

	public String getEmpanelledSince() {
		return empanelledSince;
	}

	public void setEmpanelledSince(String empanelledSince) {
		this.empanelledSince = empanelledSince;
	}

	public String getsPOCName() {
		return sPOCName;
	}

	public void setsPOCName(String sPOCName) {
		this.sPOCName = sPOCName;
	}

	public String getsPOCmobile() {
		return sPOCmobile;
	}

	public void setsPOCmobile(String sPOCmobile) {
		this.sPOCmobile = sPOCmobile;
	}

	public String getsPOCmailID() {
		return sPOCmailID;
	}

	public void setsPOCmailID(String sPOCmailID) {
		this.sPOCmailID = sPOCmailID;
	}

	public String gettPAManagerName() {
		return tPAManagerName;
	}

	public void settPAManagerName(String sPAManagerName) {
		this.tPAManagerName = sPAManagerName;
	}

	public String gettPAMMobile() {
		return tPAMMobile;
	}

	public void settPAMMobile(String tPAMMobile) {
		this.tPAMMobile = tPAMMobile;
	}

	public String gettPAMEMail() {
		return tPAMEMail;
	}

	public void settPAMEMail(String tPAMEMail) {
		this.tPAMEMail = tPAMEMail;
	}

	public String getMarketingHead() {
		return marketingHead;
	}

	public void setMarketingHead(String marketingHead) {
		this.marketingHead = marketingHead;
	}

	public String getmHMobile() {
		return mHMobile;
	}

	public void setmHMobile(String mHMobile) {
		this.mHMobile = mHMobile;
	}

	public String getmHMail() {
		return mHMail;
	}

	public void setmHMail(String mHMail) {
		this.mHMail = mHMail;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getOwnerMobile() {
		return ownerMobile;
	}

	public void setOwnerMobile(String ownerMobile) {
		this.ownerMobile = ownerMobile;
	}

	public String getOwnermail() {
		return ownermail;
	}

	public void setOwnermail(String ownermail) {
		this.ownermail = ownermail;
	}

	@Override
	public String toString() {
		return "PartnerDetail [partnerId=" + partnerId + ", providerCode=" + providerCode + ", grades=" + grades
				+ ", pANNo=" + pANNo + ", providerName=" + providerName + ", address1=" + address1 + ", address2="
				+ address2 + ", address3=" + address3 + ", location=" + location + ", cityId=" + cityId + ", distId="
				+ distId + ", stateId=" + stateId + ", Pincode=" + Pincode + ", stdCode=" + stdCode + ", landlineNo="
				+ landlineNo + ", mobileNo=" + mobileNo + ", dCLocation=" + dCLocation + ", homevisit=" + homevisit
				+ ", preferredProviderPartner=" + preferredProviderPartner + ", isActive=" + isActive
				+ ", empanelledSince=" + empanelledSince + ", sPOCName=" + sPOCName + ", sPOCmobile=" + sPOCmobile
				+ ", sPOCmailID=" + sPOCmailID + ", tPAManagerName=" + tPAManagerName + ", tPAMMobile=" + tPAMMobile
				+ ", tPAMEMail=" + tPAMEMail + ", marketingHead=" + marketingHead + ", mHMobile=" + mHMobile
				+ ", mHMail=" + mHMail + ", ownerName=" + ownerName + ", ownerMobile=" + ownerMobile + ", ownermail="
				+ ownermail + ", bankACNo=" + bankACNo + ", bankName=" + bankName + ", branchName=" + branchName
				+ ", ifscCode=" + ifscCode + ", mer=" + mer + ", ecg=" + ecg + ", pathology=" + pathology
				+ ", chestXRay=" + chestXRay + ", tmt=" + tmt + ", echo=" + echo + ", pft=" + pft + ", usg=" + usg
				+ ", urineNicotine=" + urineNicotine + "]";
	}
	
}
