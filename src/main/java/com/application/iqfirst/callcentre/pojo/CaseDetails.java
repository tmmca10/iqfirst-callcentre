package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name = "CaseDetails")
public class CaseDetails implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CaseId")
	Long 	caseId;
	
	@Column(name = "CaseCode")
	String customerCode;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CompId", nullable = false)
	InsuranceCompany InsuranceCompany;
	
	@Column(name = "PolicyNo")
	String 	policyNo;
	
	@Column(name = "CustomerName")
	String 	customerName;
	
	@Column(name = "Address")
	String 	address;
	
	@Column(name = "Pin")
	String 	pin;
	
	@Column(name = "LatLng")
	String 	latLng;
	
	@Column(name = "RegDt")
	String 	regDt;
	
	@Column(name = "CustomerCO")
	String customerCO;
	
	@Column(name = "distId")
	Long distId;
	
	@Column(name = "cityId")
	Long cityId;
	
	@Column(name = "stateId")
	Long stateId;
	
	@Column(name = "MobileNo1")
	String mobileNo1;
	
	@Column(name = "MobileNo2")
	String mobileNo2;
	
	@Column(name = "SA")
	String sa;
	
	@Column(name = "CaseType")
	String caseType;
	
	@Column(name = "PaymentBy")
	String paymentBy;
	
	/*@Column(name = "billId")
	Long billId;
	
	@Column(name = "partnerBillId")
	Long partnerBillId;*/
	
	@Column(name = "caseStatus")
	int caseStatus;
	
	@Column(name = "createdBy")
	String createdBy;
	
	@Column(name = "createddate")
	Timestamp createddate;
	
	@Column(name = "updatedby")
	String updatedby;
	
	@Column(name = "updateddate")
	Timestamp updateddate;
	
	@Column(name = "isVideoUser")
	String	isVideoUser;
	
	@ManyToMany( cascade = {
	        CascadeType.MERGE
	    })
	@JoinTable(name = "CasePkgMapping",
            joinColumns = { @JoinColumn(name = "CaseId") },
            inverseJoinColumns = { @JoinColumn(name = "PackageId") })
    Set<TestPackage> TestPackage = new HashSet<>();
	
	public CaseDetails() {}

	public CaseDetails(Long caseId, String customerCode, InsuranceCompany insuranceCompany, String policyNo,
			String customerName, String address, String pin, String latLng, String regDt, String customerCO,
			Long distId, Long cityId, Long stateId, String mobileNo1, String mobileNo2, String sa, String caseType,
			String paymentBy, Long billId, Long partnerBillId, int caseStatus, String createdBy,
			Timestamp createddate, String updatedby, Timestamp updateddate,
			Set<TestPackage> testPackage) {
		this.caseId = caseId;
		this.customerCode = customerCode;
		InsuranceCompany = insuranceCompany;
		this.policyNo = policyNo;
		this.customerName = customerName;
		this.address = address;
		this.pin = pin;
		this.latLng = latLng;
		this.regDt = regDt;
		this.customerCO = customerCO;
		this.distId = distId;
		this.cityId = cityId;
		this.stateId = stateId;
		this.mobileNo1 = mobileNo1;
		this.mobileNo2 = mobileNo2;
		this.sa = sa;
		this.caseType = caseType;
		this.paymentBy = paymentBy;
		//this.billId = billId;
		//this.partnerBillId = partnerBillId;
		this.caseStatus = caseStatus;
		this.createdBy = createdBy;
		this.createddate = createddate;
		this.updatedby = updatedby;
		this.updateddate = updateddate;
		TestPackage = testPackage;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public InsuranceCompany getInsuranceCompany() {
		return InsuranceCompany;
	}

	public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
		InsuranceCompany = insuranceCompany;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getLatLng() {
		return latLng;
	}

	public void setLatLng(String latLng) {
		this.latLng = latLng;
	}

	public String getRegDt() {
		return regDt;
	}

	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

	public String getCustomerCO() {
		return customerCO;
	}

	public void setCustomerCO(String customerCO) {
		this.customerCO = customerCO;
	}

	public Long getDistId() {
		return distId;
	}

	public void setDistId(Long distId) {
		this.distId = distId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public String getMobileNo1() {
		return mobileNo1;
	}

	public void setMobileNo1(String mobileNo1) {
		this.mobileNo1 = mobileNo1;
	}

	public String getMobileNo2() {
		return mobileNo2;
	}

	public void setMobileNo2(String mobileNo2) {
		this.mobileNo2 = mobileNo2;
	}

	public String getSa() {
		return sa;
	}

	public void setSa(String sa) {
		this.sa = sa;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getPaymentBy() {
		return paymentBy;
	}

	public void setPaymentBy(String paymentBy) {
		this.paymentBy = paymentBy;
	}

	/*public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public Long getPartnerBillId() {
		return partnerBillId;
	}

	public void setPartnerBillId(Long partnerBillId) {
		this.partnerBillId = partnerBillId;
	}*/

	public int getCaseStatus() {
		return caseStatus;
	}

	public void setCaseStatus(int caseStatus) {
		this.caseStatus = caseStatus;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Timestamp getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Timestamp updateddate) {
		this.updateddate = updateddate;
	}

	public Set<TestPackage> getTestPackage() {
		return TestPackage;
	}

	public void setTestPackage(Set<TestPackage> testPackage) {
		this.TestPackage = testPackage;
	}

	public String getIsVideoUser() {
		return isVideoUser;
	}

	public void setIsVideoUser(String isVideoUser) {
		this.isVideoUser = isVideoUser;
	}

	@Override
	public String toString() {
		return "CaseDetails [caseId=" + caseId + ", customerCode=" + customerCode + ", policyNo=" + policyNo + ", customerName=" + customerName + ", address="
				+ address + ", pin=" + pin + ", latLng=" + latLng + ", regDt=" + regDt + ", customerCO=" + customerCO
				+ ", distId=" + distId + ", cityId=" + cityId + ", stateId=" + stateId + ", mobileNo1=" + mobileNo1
				+ ", mobileNo2=" + mobileNo2 + ", sa=" + sa + ", caseType=" + caseType + ", paymentBy=" + paymentBy
				+ ", caseStatus=" + caseStatus + ", createdBy=" + createdBy + ", createddate=" + createddate
				+ ", updatedby=" + updatedby + ", updateddate=" + updateddate + ", isVideoUser=" + isVideoUser
				+ ", TestPackage=" + TestPackage + "]";
	}
	
	
	
}