package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "CasePartnerMapping")
public class CasePartnerMapping implements Serializable{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "mappingId")
	private Long mappingId;
	
	@Column(name = "casedetailsId")
	private Long casedetailsId;
	
	@Column(name = "partnerId")
	private Long partnerId;
	
	@Column(name = "scheduledDate")
	private Timestamp scheduledDate;
	
	@Column(name = "isHomeVisit")
	private Character isHomeVisit;
	
	@Column(name = "createdDate")
	private Timestamp createdDate;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "modifiedDate")
	private Timestamp modifiedDate;
	
	@Column(name = "modifiedBy")
	private String modifiedBy;
	
	public CasePartnerMapping() {}

	public CasePartnerMapping(Long mappingId, Long casedetailsId, Long partnerId, Timestamp scheduledDate,
			Character isHomeVisit, Timestamp createdDate, String createdBy, Timestamp modifiedDate, String modifiedBy) {
		this.mappingId = mappingId;
		this.casedetailsId = casedetailsId;
		this.partnerId = partnerId;
		this.scheduledDate = scheduledDate;
		this.isHomeVisit = isHomeVisit;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
	}

	public Long getMappingId() {
		return mappingId;
	}

	public void setMappingId(Long mappingId) {
		this.mappingId = mappingId;
	}

	public Long getCasedetailsId() {
		return casedetailsId;
	}

	public void setCasedetailsId(Long casedetailsId) {
		this.casedetailsId = casedetailsId;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public Timestamp getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(Timestamp scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public Character getIsHomeVisit() {
		return isHomeVisit;
	}

	public void setIsHomeVisit(Character isHomeVisit) {
		this.isHomeVisit = isHomeVisit;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public String toString() {
		return "CasePartnerMapping [mappingId=" + mappingId + ", casedetailsId=" + casedetailsId + ", partnerId="
				+ partnerId + ", scheduledDate=" + scheduledDate + ", isHomeVisit=" + isHomeVisit + ", createdDate="
				+ createdDate + ", createdBy=" + createdBy + ", modifiedDate=" + modifiedDate + ", modifiedBy="
				+ modifiedBy + "]";
	}
	
}
