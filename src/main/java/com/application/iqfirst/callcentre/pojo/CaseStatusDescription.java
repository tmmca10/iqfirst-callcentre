package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "caseStatusDescription")
public class CaseStatusDescription implements Serializable{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "statusId")
	int statusId;
	
	@Column(name = "statusName")
	String statusName;
	
	@Column(name = "updatedBy")
	String updatedBy;
	
	@Column(name = "updatedDate")
	Timestamp updatedDate;
	
	protected CaseStatusDescription(){}

	public int getStatusId() {
		return statusId;
	}

	public String getStatusName() {
		return statusName;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	@Override
	public String toString() {
		return "CaseStatusDescription [statusId=" + statusId + ", statusName=" + statusName + ", updatedBy=" + updatedBy
				+ ", updatedDate=" + updatedDate + "]";
	}
	
}
