package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "CasePkgMapping")
public class CasePkgMapping implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MappingId")
	private Long mappingId;
	
	@Column(name = "CaseId")
	private Long caseId;
	
	@Column(name = "PackageId")
	private Long packageId;

	public CasePkgMapping(){}
	
	public CasePkgMapping(Long mappingId, Long caseId, Long packageId) {
		this.mappingId = mappingId;
		this.caseId = caseId;
		this.packageId = packageId;
	}
	
	public CasePkgMapping(Long caseId, Long packageId) {
		this.caseId = caseId;
		this.packageId = packageId;
	}

	public Long getMappingId() {
		return mappingId;
	}

	public void setMappingId(Long mappingId) {
		this.mappingId = mappingId;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Long getPackageId() {
		return packageId;
	}

	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	@Override
	public String toString() {
		return "CasePkgMapping [mappingId=" + mappingId + ", caseId=" + caseId + ", packageId=" + packageId + "]";
	}
	
}
