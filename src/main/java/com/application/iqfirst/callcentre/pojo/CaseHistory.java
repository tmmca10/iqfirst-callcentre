package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "casehistory")
public class CaseHistory implements Serializable{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "historyId")
	Long historyId;
	
	@Column(name = "statusId")
	int statusId;
	
	@Column(name = "caseId")
	Long caseId;
	
	@Column(name = "description")
	String description;
	
	@Column(name = "updatedDate")
	Timestamp updatedDate;
	
	@Column(name = "updatedBy")
	String updatedBy;
	
	public CaseHistory() {
		// TODO Auto-generated constructor stub
	}

	public CaseHistory(int statusId, Long caseId, String description, Timestamp updatedDate,
			String updatedBy) {
		this.statusId = statusId;
		this.caseId = caseId;
		this.description = description;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
	}

	public Long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(Long historyId) {
		this.historyId = historyId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "CaseHistory [historyId=" + historyId + ", statusId=" + statusId + ", caseId=" + caseId
				+ ", description=" + description + ", updatedDate=" + updatedDate + ", updatedBy=" + updatedBy + "]";
	}
}
