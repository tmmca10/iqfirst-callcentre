package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name = "vcslots")
public class VcSlots implements Serializable{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "slotId")
	Long slotId;
	
	@Column(name = "caseId")
	Long caseId;
	
	@Column(name = "assignedto")
	Long assignedTo;
	
	@Column(name = "conferencedate")
	@Temporal(TemporalType.DATE)
	Date conferenceDate;
	
	@Column(name = "starttime")
	String startTime;
	
	@Column(name = "endtime")
	String endTime;
	
	@Column(name = "assigendby")
	String assignedBy;
	
	@Column(name = "assigneddate")
	Timestamp assignedDate;
	
	@Column(name = "updatedby")
	String updatedBy;
	
	@Column(name = "updateddate")
	Timestamp updatedDate;
	
	public VcSlots(){}

	public VcSlots(Long slotId, Long caseId, 
			Long assignedTo, Date conferenceDate, 
			String startTime, String endTime,
			String updatedBy, Timestamp updatedDate) {
		this.slotId = slotId;
		this.caseId = caseId;
		this.conferenceDate = conferenceDate;
		this.startTime = startTime;
		this.endTime = endTime;
		this.assignedTo = assignedTo;
		this.assignedBy = updatedBy;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}
	
	public VcSlots(Long caseId, Long assignedTo, Date conferenceDate, String startTime, String endTime, String assignedBy,
			Timestamp assignedDate) {
		this.caseId = caseId;
		this.conferenceDate = conferenceDate;
		this.startTime = startTime;
		this.endTime = endTime;
		this.assignedTo = assignedTo;
		this.assignedBy = assignedBy;
		this.assignedDate = assignedDate;
	}


	public Long getSlotId() {
		return slotId;
	}

	public void setSlotId(Long slotId) {
		this.slotId = slotId;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Date getConferenceDate() {
		return conferenceDate;
	}

	public void setConferenceDate(Date conferenceDate) {
		this.conferenceDate = conferenceDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Long getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getAssignedBy() {
		return assignedBy;
	}

	public void setAssignedBy(String assignedBy) {
		this.assignedBy = assignedBy;
	}

	public Timestamp getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(Timestamp assignedDate) {
		this.assignedDate = assignedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "VcSlots [slotId=" + slotId + ", caseId=" + caseId + ", assignedTo=" + assignedTo + ", conferenceDate="
				+ conferenceDate + ", startTime=" + startTime + ", endTime=" + endTime + ", assignedBy=" + assignedBy
				+ ", assignedDate=" + assignedDate + ", updatedBy=" + updatedBy + ", updatedDate=" + updatedDate + "]";
	}

}