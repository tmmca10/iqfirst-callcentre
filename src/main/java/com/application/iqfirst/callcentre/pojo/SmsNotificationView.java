package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "SmsNotificationView")
public class SmsNotificationView implements Serializable{
	
	@Column(name = "CaseId")
	@Id
	Long 	CaseId;
	
	@Column(name = "CustomerName")
	String CustomerName;
	
	@Column(name = "CustomerCode")
	String CustomerCode;
	
	@Column(name = "CustomersContact")
	String	CustomersContact;
	
	@Column(name = "Partner")
	String ProviderName;
	
	@Column(name = "PartnersContact")
	String PartnersContact;
	
	@Column(name = "Packages")
	String packageCd;
	
	@Column(name = "ScheduledDate")
	String scheduledDate;
	
	@Column(name="CurrentStatus")
	String currentStatus;
	
	@Column(name = "AllocatedTo")
	String assignedTo;
	
	@Column(name = "UsersContact")
	String usersContact;
	
	public SmsNotificationView(){}
	
	public SmsNotificationView(Long caseId, String customerName, String customerCode, String customersContact,
			String providerName, String partnersContact, String packageCd, String scheduledDate, String currentStatus,
			String assignedTo, String usersContact) {
		this.CaseId = caseId;
		this.CustomerName = customerName;
		this.CustomerCode = customerCode;
		this.CustomersContact = customersContact;
		this.ProviderName = providerName;
		this.PartnersContact = partnersContact;
		this.packageCd = packageCd;
		this.scheduledDate = scheduledDate;
		this.currentStatus = currentStatus;
		this.assignedTo = assignedTo;
		this.usersContact = usersContact;
	}

	public Long getCaseId() {
		return CaseId;
	}

	public void setCaseId(Long caseId) {
		CaseId = caseId;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getCustomerCode() {
		return CustomerCode;
	}

	public void setCustomerCode(String customerCode) {
		CustomerCode = customerCode;
	}

	public String getCustomersContact() {
		return CustomersContact;
	}

	public void setCustomersContact(String customersContact) {
		CustomersContact = customersContact;
	}

	public String getProviderName() {
		return ProviderName;
	}

	public void setProviderName(String providerName) {
		ProviderName = providerName;
	}

	public String getPartnersContact() {
		return PartnersContact;
	}

	public void setPartnersContact(String partnersContact) {
		PartnersContact = partnersContact;
	}

	public String getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(String scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public String getPackageCd() {
		return packageCd;
	}

	public void setPackageCd(String packageCd) {
		this.packageCd = packageCd;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getUsersContact() {
		return usersContact;
	}

	public void setUsersContact(String usersContact) {
		this.usersContact = usersContact;
	}

	@Override
	public String toString() {
		return "SmsNotificationView [CaseId=" + CaseId + ", CustomerName=" + CustomerName + ", CustomerCode="
				+ CustomerCode + ", CustomersContact=" + CustomersContact + ", ProviderName=" + ProviderName
				+ ", PartnersContact=" + PartnersContact + ", packageCd=" + packageCd + ", scheduledDate="
				+ scheduledDate + ", currentStatus=" + currentStatus + ", assignedTo=" + assignedTo + ", usersContact="
				+ usersContact + "]";
	}
}
