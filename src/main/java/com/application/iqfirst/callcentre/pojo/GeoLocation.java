package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "geo_locations")
public class GeoLocation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "sdcId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long sdcId;
	@Column(name = "name")
	private String name;
	@Column(name = "type")
	private String type;
	@Column(name = "pin")
	private String pin;
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "parent")
	private GeoLocation parent;

	/*
	 * @OneToMany(mappedBy = "parent") private List<GeoLocation> child = new
	 * ArrayList<>();
	 */

	public Long getSdcId() {
		return sdcId;
	}

	public void setSdcId(Long sdcId) {
		this.sdcId = sdcId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public GeoLocation getParent() {
		return parent;
	}

	public void setParent(GeoLocation parent) {
		this.parent = parent;
	}

	/*
	 * public List<GeoLocation> getChild() { return child; }
	 * 
	 * public void setChild(List<GeoLocation> child) { this.child = child; }
	 */

}
