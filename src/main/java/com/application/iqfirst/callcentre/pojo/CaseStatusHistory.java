package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "casestatushistory")
public class CaseStatusHistory implements Serializable {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "statusid")
	private Long statusid;
	
	@Column(name = "casedetailsid")
	private Long casedetailsid;
	
	@Column(name = "partnerid")
	private Long partnerid;
	
	@Column(name = "updateddate")
	private Timestamp updateddate;
	
	@Column(name = "updatedby")
	private String updatedby;
	
	public CaseStatusHistory() {}
	
	public CaseStatusHistory(Long statusid, Long casedetailsid, Long partnerid, Timestamp updateddate,
			String updatedby) {
		this.statusid = statusid;
		this.casedetailsid = casedetailsid;
		this.partnerid = partnerid;
		this.updateddate = updateddate;
		this.updatedby = updatedby;
	}

	public Long getStatusid() {
		return statusid;
	}

	public void setStatusid(Long statusid) {
		this.statusid = statusid;
	}

	public Long getCasedetailsid() {
		return casedetailsid;
	}

	public void setCasedetailsid(Long casedetailsid) {
		this.casedetailsid = casedetailsid;
	}

	public Long getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(Long partnerid) {
		this.partnerid = partnerid;
	}

	public Timestamp getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Timestamp updateddate) {
		this.updateddate = updateddate;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	@Override
	public String toString() {
		return "CaseStatusHistory [statusid=" + statusid + ", casedetailsid=" + casedetailsid + ", partnerid="
				+ partnerid + ", updateddate=" + updateddate + ", updatedby=" + updatedby + "]";
	}
}
