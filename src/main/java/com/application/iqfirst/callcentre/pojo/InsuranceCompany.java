package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.application.iqfirst.callcentre.pojo.CaseDetails;
import com.application.iqfirst.callcentre.pojo.TestPackage;
import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings("serial")
@Entity
@Table(name = "InsuranceCompany")
public class InsuranceCompany implements Serializable{
	
	@Column(name = "CompId")
	@Id
	Long 	compId;
	
	@Column(name = "CompName")
	String 	compName;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "InsuranceCompany")
	@JsonIgnore
	Set<CaseDetails> CaseDetails = new HashSet<CaseDetails>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "InsuranceCompany")
	@JsonIgnore
	Set<TestPackage> TestPackage = new HashSet<TestPackage>(0);

	public InsuranceCompany(){}
	
	public InsuranceCompany(Long compId, String compName) {
		this.compId = compId;
		this.compName = compName;
	}

	public Long getCompId() {
		return compId;
	}

	public void setCompId(Long compId) {
		this.compId = compId;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	public Set<CaseDetails> getCaseDetails() {
		return CaseDetails;
	}

	public void setCaseDetails(Set<CaseDetails> caseDetails) {
		CaseDetails = caseDetails;
	}

	@Override
	public String toString() {
		return "InsuranceCompany [compId=" + compId + ", compName=" + compName + ", CaseDetails=" + CaseDetails + "]";
	}

}
