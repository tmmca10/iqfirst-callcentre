package com.application.iqfirst.callcentre.pojo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.application.iqfirst.callcentre.pojo.CaseDetails;
import com.application.iqfirst.callcentre.pojo.InsuranceCompany;

@SuppressWarnings("serial")
@Entity
@Table(name = "TestPackage", uniqueConstraints = {
		@UniqueConstraint(columnNames = "PackageId"),
		@UniqueConstraint(columnNames = "PackageCd") })
public class TestPackage implements Serializable{
	
	@Column(name = "PackageId")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long packageId;
	
	@Column(name = "PackageCd")
	String packageCd;
	
	@Column(name = "PackageTests")
	String packageTests;
	
	@ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            },
            mappedBy = "TestPackage")
    Set<CaseDetails> CaseDetails = new HashSet<>();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "InsComId", nullable = false)
	InsuranceCompany InsuranceCompany;

	public TestPackage(){}

	public TestPackage(Long packageId, String packageCd, String packageTests,
			Set<CaseDetails> caseDetails,
			InsuranceCompany insuranceCompany) {
		this.packageId = packageId;
		this.packageCd = packageCd;
		this.packageTests = packageTests;
		CaseDetails = caseDetails;
		InsuranceCompany = insuranceCompany;
	}

	public Long getPackageId() {
		return packageId;
	}

	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	public String getPackageCd() {
		return packageCd;
	}

	public void setPackageCd(String packageCd) {
		this.packageCd = packageCd;
	}

	public String getPackageTests() {
		return packageTests;
	}

	public void setPackageTests(String packageTests) {
		this.packageTests = packageTests;
	}

	public Set<CaseDetails> getCaseDetails() {
		return CaseDetails;
	}

	public void setCaseDetails(Set<CaseDetails> caseDetails) {
		CaseDetails = caseDetails;
	}

	public InsuranceCompany getInsuranceCompany() {
		return InsuranceCompany;
	}

	public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
		InsuranceCompany = insuranceCompany;
	}

	@Override
	public String toString() {
		return "TestPackage [packageId=" + packageId + ", packageCd=" + packageCd + ", packageTests=" + packageTests + "]";
	}
	
}
