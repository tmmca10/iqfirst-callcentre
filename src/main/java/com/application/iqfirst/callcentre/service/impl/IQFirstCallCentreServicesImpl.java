package com.application.iqfirst.callcentre.service.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.application.iqfirst.callcentre.dto.CaseDetailsDTO;
import com.application.iqfirst.callcentre.dto.CasePartnerMappingDTO;
import com.application.iqfirst.callcentre.dto.PartnerDetailDTO;
import com.application.iqfirst.callcentre.pojo.CaseDetails;
import com.application.iqfirst.callcentre.pojo.CasePartnerMapping;
import com.application.iqfirst.callcentre.pojo.GeoLocation;
import com.application.iqfirst.callcentre.pojo.PartnerDetail;
import com.application.iqfirst.callcentre.pojo.SmsNotificationView;
import com.application.iqfirst.callcentre.pojo.VcSlots;
import com.application.iqfirst.callcentre.repository.IQFirstCasePartnerMappingRepository;
import com.application.iqfirst.callcentre.repository.IQFirstCaseStatusHistoryRepository;
import com.application.iqfirst.callcentre.repository.IQFirstGeoLocationRepository;
import com.application.iqfirst.callcentre.repository.IQFirstCaseDetailsRepository;
import com.application.iqfirst.callcentre.repository.IQFirstPartnerDetailRepository;
import com.application.iqfirst.callcentre.repository.SmsNotificationRepository;
import com.application.iqfirst.callcentre.repository.VcSlotsRepository;
import com.application.iqfirst.callcentre.service.IQFirstCallCentreServices;
import com.application.iqfirst.callcentre.util.CallCentreUtil;
import com.application.iqfirst.callcentre.util.DTOMapper;
import com.application.iqfirst.callcentre.util.QueryBuilder;
import com.application.iqfirst.callcentre.util.StatusValues;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class IQFirstCallCentreServicesImpl implements IQFirstCallCentreServices {
	
	private static final Logger log = LoggerFactory.getLogger(IQFirstCallCentreServicesImpl.class);
	
	@Autowired
	IQFirstCaseDetailsRepository 			iQFirstCaseDetailsRepository;
	
	@Autowired
	IQFirstPartnerDetailRepository 			iQFirstPartnerDetailRepository;
	
	@Autowired
	IQFirstGeoLocationRepository 			iQGeoLocationRepository;
	
	@Autowired
	IQFirstCasePartnerMappingRepository		iQFirstCasePartnerMappingRepository;
	
	@Autowired
	IQFirstCaseStatusHistoryRepository 		iQFirstCaseStatusHistoryRepository;
	
	@Autowired
	SmsNotificationRepository				smsNotificationRepository;
	
	@Autowired
	VcSlotsRepository						vcSlotsRepository;
	
	@Autowired
	DTOMapper 		dTOMapper;
	
	@Autowired
	QueryBuilder 	queryBuilder;
	
	@Autowired
	CallCentreUtil callCentreUtil;
	
	@Value("${template.usernotification}")
	String usernotification;
	
	@Value("${template.userreportnotification}")
	String userReportNotification;
	
	@Value("${template.dcnotification}")
	String dcNotification;
	
	@Value("${template.dcusernotification}")
	String dcUserNotification;
	
	@Value("${sms.url.service}")
    private String smsUrl;
	
	@Value("${mail.url.service}")
    private String mailUrl;
	
	@Override
	public ResponseEntity<?> findAllCases(HttpHeaders headers, Map<String, String> map) {
		if(headers.get("UserName") == null){
			log.error("username not found in Header information in findAllCases method");
			return new ResponseEntity<String>("Un-authorized access", HttpStatus.UNAUTHORIZED);
		}
		try{
			List<CaseDetails> filteredCaseDetails = queryBuilder.getAllCaseDetailsByCriteria(map);
			List<CaseDetailsDTO> getAllCaseDetails = new ArrayList<>();
			
			filteredCaseDetails.stream().forEach(caseDetails -> getAllCaseDetails.add(dTOMapper.caseDetailstoDTO(caseDetails)));
			
			return new ResponseEntity<List<CaseDetailsDTO>>(getAllCaseDetails, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<String>("Problem in findAllCases method. Error: "+e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@Override
	public List<Map<Long,String>> getChilds(Long sdcId) {
		List<Map<Long,String>> distList = new ArrayList<>();
		List<GeoLocation> districts = iQGeoLocationRepository.findChilds(sdcId);
		for(GeoLocation geoLocation : districts){
			Map<Long,String> geoMap = Collections.singletonMap(geoLocation.getSdcId(),geoLocation.getName());
			distList.add(geoMap);
		}
		return distList;
	}

	@Override
	public List<String> getAllPinCodesOfCity(Long id) {
		List<Object[]> pincodes = iQFirstPartnerDetailRepository.findPinCodes(id);
		List<String> getAllPinCodesOfCity = pincodes.stream()
        .map(objects -> {
            String pinCode = String.valueOf(objects[0]);
            return pinCode;
        })
        .collect(Collectors.toList());
		return getAllPinCodesOfCity;
	}

	@Override
	public ResponseEntity<?> findAllRelatedPartners(HttpHeaders headers, Map<String, String> map) {
		if(headers.get("UserName") == null){
			log.error("username not found in Header information in findAllCases method");
			return new ResponseEntity<String>("Un-authorized access", HttpStatus.UNAUTHORIZED);
		}
		try{
			List<PartnerDetail> partnersList = queryBuilder.findPartnersByCriteria(map);
			List<PartnerDetailDTO> relatedDC = new ArrayList<PartnerDetailDTO>();
			
			partnersList.stream().forEach(partnerDetail -> relatedDC.add(dTOMapper.partnersDetailsToDTO(partnerDetail)));
			
			return new ResponseEntity<List<PartnerDetailDTO>>(relatedDC, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<String>("Problem encounter in findAllRelatedPartners. Error: "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);// TODO: handle exception
		}
	}
	
	@Override
	public ResponseEntity<?> assignCaseToPartners(HttpHeaders headers, CasePartnerMappingDTO casePartnerMappingDTO) {
		String userName="";
		if(headers.get("UserName") == null){
			return new ResponseEntity<String>("Un-authorized access", HttpStatus.UNAUTHORIZED);
		}else{
			userName = headers.get("UserName").toString().replace("[", "").replace("]", "");
		}
		List<CasePartnerMapping> existingCase = iQFirstCasePartnerMappingRepository.findByCaseID(casePartnerMappingDTO.getCasedetailsId());
		if(existingCase.size() > 0){
			if(casePartnerMappingDTO.getIsHomeVisit() != null){
				existingCase.get(0).setIsHomeVisit(casePartnerMappingDTO.getIsHomeVisit());
			}
			existingCase.get(0).setPartnerId(casePartnerMappingDTO.getPartnerId());
			existingCase.get(0).setScheduledDate(Timestamp.valueOf(casePartnerMappingDTO.getScheduledDate()));
			return updateCasePartnerMapping(headers, dTOMapper.casePartnerToDTO(existingCase.get(0)));
		}
		try{
			saveCaseToPartners(userName, casePartnerMappingDTO);
		}catch (Exception e) {
			log.error("There is an error occured while assigning this case to partner. Error: "+e.getMessage());
			return new ResponseEntity<String>("There is an error occured while assigning this case to partner", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(!sendNotification(casePartnerMappingDTO, usernotification, dcNotification, headers)){
			return new ResponseEntity<String>("Case is now assigned to: "+casePartnerMappingDTO.getProviderName()+" , but unable to send SMS or Mail notification", HttpStatus.OK);
		}
		return new ResponseEntity<String>("Case is now assigned to: "+casePartnerMappingDTO.getProviderName(), HttpStatus.OK);
	}
	
	@Transactional
	public void saveCaseToPartners(String userName, CasePartnerMappingDTO casePartnerMappingDTO) throws ParseException{
		try{
			casePartnerMappingDTO = callCentreUtil.processCasePartnerMappingDTO(userName,casePartnerMappingDTO, "create");
			CasePartnerMapping casePartnerMapping = dTOMapper.dtoTocasePartnerMapping(casePartnerMappingDTO);
			iQFirstCasePartnerMappingRepository.save(casePartnerMapping);
		}catch (Exception e) {
			log.error("Problem encounter in callcenterservice at saveCaseToPartners method. Error: "+e.getMessage());// TODO: handle exception
		}
	}

	@Override
	public ResponseEntity<?> updateCasePartnerMapping(HttpHeaders headers, CasePartnerMappingDTO casePartnerMappingDTO) {
		String userName="";
		if(headers.get("UserName") == null){
			return new ResponseEntity<String>("Un-authorized access", HttpStatus.UNAUTHORIZED);
		}else{
			userName = headers.get("UserName").toString().replace("[", "").replace("]", "");
		}
		CasePartnerMapping casePartnerMapping = iQFirstCasePartnerMappingRepository.findByID(casePartnerMappingDTO.getMappingId());
		casePartnerMapping.setCasedetailsId(casePartnerMappingDTO.getCasedetailsId());
		casePartnerMapping.setPartnerId(casePartnerMappingDTO.getPartnerId());
		if(casePartnerMappingDTO.getScheduledDate() != null){
			casePartnerMapping.setScheduledDate(Timestamp.valueOf(casePartnerMappingDTO.getScheduledDate()));
		}
		casePartnerMapping.setModifiedDate(new Timestamp(System.currentTimeMillis()));
		casePartnerMapping.setModifiedBy(userName);
		if(casePartnerMappingDTO.getIsHomeVisit() != null){
			casePartnerMapping.setIsHomeVisit(casePartnerMappingDTO.getIsHomeVisit());
		}
		casePartnerMappingDTO = dTOMapper.casePartnerToDTO(casePartnerMapping, casePartnerMappingDTO);
		casePartnerMappingDTO.setScheduledDate(casePartnerMapping.getScheduledDate().toString());
		try{
			updateCaseToPartner(userName, casePartnerMapping, casePartnerMappingDTO);
		}catch (Exception e) {
			log.error("Problem encounter in Callcentreservice at updateCasePartnerMapping. Error: "+e.getMessage());
			return new ResponseEntity<String>("There is an error occured while updating this process", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if(!sendNotification(casePartnerMappingDTO, usernotification, dcNotification, headers)){
			log.info("The Process has been updated for this Case, but unable to send SMS/ mail notification");
			return new ResponseEntity<String>("The Process has been updated for this Case, but unable to send SMS/ mail notification", HttpStatus.OK);
		}
		return new ResponseEntity<String>("The Process has been updated for this Case", HttpStatus.OK);
	}
	
	@Transactional
	public void updateCaseToPartner(String userName, CasePartnerMapping casePartnerMapping, CasePartnerMappingDTO casePartnerMappingDTO){
		try{
			casePartnerMappingDTO = callCentreUtil.processCasePartnerMappingDTO(userName, casePartnerMappingDTO, "update");
			iQFirstCasePartnerMappingRepository.save(casePartnerMapping);
		}catch (Exception e) {
			log.error("Probelm encounter in callcentreservice at updateCaseToPartner method. Error: "+e.getMessage());
		}
	}

	@Override
	public ResponseEntity<?> getAllCasePartnersMapping(HttpHeaders headers, Long mappingId) {
		String userName="";
		if(headers.get("UserName") == null){
			return new ResponseEntity<String>("Un-authorized access", HttpStatus.UNAUTHORIZED);
		}else{
			userName = headers.get("UserName").toString().replace("[", "").replace("]", "");
		}
		try{
			List<CasePartnerMappingDTO> mappingList = new ArrayList<>();;
			if(mappingId != null){
				mappingList.clear();
				CasePartnerMapping casePartnerMapping = iQFirstCasePartnerMappingRepository.findByID(mappingId);
				mappingList.add(callCentreUtil.processCasePartnerMappingDTO(userName, dTOMapper.casePartnerToDTO(casePartnerMapping), "get"));
			}else{
				mappingList.clear(); 
				List<CasePartnerMapping> casePartnerMappings = iQFirstCasePartnerMappingRepository.findAllCasePartnersMapping();
				casePartnerMappings.stream().forEach(casePartnerMapping -> mappingList.add(callCentreUtil.processCasePartnerMappingDTO(headers.get("UserName").toString().replace("[", "").replace("]", ""), dTOMapper.casePartnerToDTO(casePartnerMapping), "get")));
			}
			return new ResponseEntity<List<CasePartnerMappingDTO>>(mappingList, HttpStatus.OK);
		}catch (Exception e) {
			log.error("Problem encounter in callcentreservice at getAllCasePartnersMapping method. Error: "+e.getMessage());
			return new ResponseEntity<String>("Problem encounter in callcentreservice at getAllCasePartnersMapping method.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<?> updatePartnerStatus(HttpHeaders headers, Long partnerId, Character isActive) {
		if(headers.get("UserName") == null){
			return new ResponseEntity<String>("Un-authorized access", HttpStatus.UNAUTHORIZED);
		}
		
		if(partnerId == null || isActive == null){
			return new ResponseEntity<String>("To update Partner PartnerId must have to send as parameter along with the status Key", HttpStatus.BAD_REQUEST);
		}
		try{
			PartnerDetail partner = iQFirstPartnerDetailRepository.findByPartnerId(partnerId);
			partner.setIsActive(isActive);
			iQFirstPartnerDetailRepository.save(partner);
			if(isActive.equals('Y') || isActive.equals('y')){
				return new ResponseEntity<String>("'"+partner.getProviderName()+"' is now Active.",HttpStatus.OK);
			}else if(isActive.equals('N') || isActive.equals('n')){
				return new ResponseEntity<String>("'"+partner.getProviderName()+"' is now De-Activated.",HttpStatus.OK);
			}else{
				return new ResponseEntity<String>("Wrong input given to active or deactive Partner",HttpStatus.BAD_REQUEST);
			}
		}catch (Exception e) {
			log.error("Probelm encountered in callcentreservice at updatePartnerStatus method. Error: "+e.getMessage());
			return new ResponseEntity<String>("Probelm encountered in callcentreservice at updatePartnerStatus method.",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public boolean sendNotification(CasePartnerMappingDTO cpmd, String templateName1, String templateName2, HttpHeaders headers){
		boolean isNotificationSend = false;
		try{
			HttpEntity<String> caseSms = prepareSmsForCase(cpmd, templateName1, headers);
			Map<String, String> caseSmsMap = sendSms(smsUrl, caseSms);
			HttpEntity<String> dcSms = prepareSmsForDC(cpmd, templateName2, headers);
			Map<String, String> dcSmsMap = sendSms(smsUrl, dcSms);
			ResponseEntity<?> mailSender = callCentreUtil.sendEmail(cpmd, mailUrl);
			if(caseSmsMap.containsKey("Status") && dcSmsMap.containsKey("Status")){
				if(caseSmsMap.get("Status").equals("Success") && dcSmsMap.get("Status").equals("Success") && mailSender.getStatusCodeValue()==200){
					isNotificationSend = true;
				}
			}
		}catch (Exception e) {
			log.error("Problem encounter in callcenterservice at sendNotification method. Error: "+e.getMessage());
		}
		return isNotificationSend;
	}
	
	public HttpEntity<String> prepareSmsForCase(CasePartnerMappingDTO cpmd, String templateName, HttpHeaders headers){
		SmsNotificationView snv = smsNotificationRepository.getDetailsForNotification(cpmd.getCasedetailsId());
		JSONObject userObj = new JSONObject();
    	JSONArray arr=new JSONArray();
    	arr.put(snv.getCustomerName());
    	arr.put(snv.getProviderName());
    	String[] timing = snv.getScheduledDate().split(" ");
    	arr.put(timing[0]);
    	arr.put(timing[1]);
    	arr.put(snv.getPackageCd());
    	try {
			userObj.put("mobileNo", snv.getCustomersContact());
			userObj.put("templateName",templateName );
			userObj.put("params", arr);
		} catch (JSONException e) {
			log.error("Problem encountered callcenterservice at prepareSmsForCase method. Error: "+e.getMessage());
			e.printStackTrace();
		}
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	HttpEntity<String> request = new HttpEntity<String>(userObj.toString(), headers);
    	return request;
	}
	
	public HttpEntity<String> prepareSmsForDC(CasePartnerMappingDTO cpmd, String templateName, HttpHeaders headers){
		SmsNotificationView snv = smsNotificationRepository.getDetailsForNotification(cpmd.getCasedetailsId());
		JSONObject userObj = new JSONObject();
    	JSONArray arr=new JSONArray();
    	arr.put(snv.getProviderName());
    	arr.put(snv.getCustomerCode());
    	String[] timing = snv.getScheduledDate().split(" ");
    	arr.put(timing[0]);
    	arr.put(timing[1]);
    	arr.put(snv.getPackageCd());
    	try {
			userObj.put("mobileNo", snv.getPartnersContact());
			userObj.put("templateName",templateName );
			userObj.put("params", arr);
		} catch (JSONException e) {
			log.error("Problem encounter in callcentreservice at prepareSmsForDC method. error: "+e.getMessage());
			e.printStackTrace();
		}
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	HttpEntity<String> request = new HttpEntity<String>(userObj.toString(), headers);
    	return request;
	}
	
	@SuppressWarnings({ "unchecked" })
	public Map<String, String> sendSms(String smsUrl, HttpEntity<String> request){
		RestTemplate template=new RestTemplate();
		ResponseEntity<String> response=template.postForEntity(smsUrl, request, String.class);
		String body=response.getBody();
    	Map<String, String> map=new HashMap<>();
    	try {
			map=new ObjectMapper().readValue(body, Map.class);
		} catch (JsonMappingException e) {
			log.error("Problem encountered in callcentrecervice at sendSms method(JsonMappingException). Error : "+e.getMessage());
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			log.error("Problem encountered in callcentrecervice at sendSms method(JsonProcessingException). Error : "+e.getMessage());
			e.printStackTrace();
		}
    	return map;
	}

	@Override
	public ResponseEntity<?> assignCaseToVc(HttpHeaders headers, Map<String, String> map){
		if(headers.get("UserName") == null){
			return new ResponseEntity<String>("Un-authorized access", HttpStatus.UNAUTHORIZED);
		}
		
		VcSlots vcSlots = null;
		
		StringBuffer startTime 	= new StringBuffer();
		StringBuffer endTime 	= new StringBuffer();
		Date scheduledDate 		= null;
		
		if(!map.containsKey("caseId") && !map.containsKey("assignedto") && !map.containsKey("conferencedate")
				&& !map.containsKey("starttime") && !map.containsKey("endtime")){
			return new ResponseEntity<String>("Not All parameters present to assign the case to VC", HttpStatus.BAD_REQUEST);
			
		}else{
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try{
				if(map.get("conferencedate") == null && map.get("conferencedate").isEmpty()){
					return new ResponseEntity<String>("Conference date can not be blank", HttpStatus.BAD_REQUEST);
				}else{
					scheduledDate = (Date)formatter.parse(map.get("conferencedate"));
				}
				
				if(map.get("starttime") == null && map.get("starttime").isEmpty()){
					return new ResponseEntity<String>("Conference start time can not be blank", HttpStatus.BAD_REQUEST);
				}else{
					if(map.get("starttime").length()==5){
						startTime.append(map.get("starttime"));
						startTime.append(":00");
					}else{
						startTime.append(map.get("starttime"));
					}
				}
				
				if(map.get("endtime") == null && map.get("endtime").isEmpty()){
					return new ResponseEntity<String>("Conference end time can not be blank", HttpStatus.BAD_REQUEST);
				}else{
					if(map.get("endtime").length()==5){
						endTime.append(map.get("endtime"));
						endTime.append(":00");
					}else{
						endTime.append(map.get("endtime"));
					}
				}
				
				List<VcSlots> allSlots = vcSlotsRepository.findConferenceByDateTime(scheduledDate, Long.parseLong(map.get("assignedto")));
				for(VcSlots vc : allSlots){
					if(checkTimeForOverLap(vc.getStartTime(), vc.getEndTime(), startTime.toString(), endTime.toString())){
						return new ResponseEntity<String>("User already have an sheduled appointment on provided date and time. Please choose different time", HttpStatus.NOT_ACCEPTABLE);
					}
				}
				log.info("Request MAp for video case assign "+map); 
				if(map.containsKey("slotId") && StringUtils.isNotBlank(map.get("slotId"))){
					vcSlots = new VcSlots(Long.parseLong(map.get("slotId")), 
							Long.parseLong(map.get("caseId")), 
							Long.parseLong(map.get("assignedto")),
							scheduledDate, 
							startTime.toString(),
							endTime.toString(), 
							headers.get("UserName").toString().replace("[", "").replace("]", ""),
							new Timestamp(System.currentTimeMillis()));
					log.info("Updating vcSlots");
					callCentreUtil.updateCaseDetails(Long.parseLong(map.get("caseId")), headers.get("UserName").toString().replace("[", "").replace("]", ""));
					callCentreUtil.updateCaseHistory(StatusValues.ASSIGNED, Long.parseLong(map.get("caseId")), "video conference details updated for this case", headers.get("UserName").toString().replace("[", "").replace("]", ""));
				}else{
					vcSlots = new VcSlots(Long.parseLong(map.get("caseId")), Long.parseLong(map.get("assignedto")), scheduledDate, startTime.toString(), endTime.toString(), 
							headers.get("UserName").toString().replace("[", "").replace("]", ""), new Timestamp(System.currentTimeMillis()));
					log.info("inserting vcSlots");
					callCentreUtil.updateCaseDetails(Long.parseLong(map.get("caseId")), headers.get("UserName").toString().replace("[", "").replace("]", ""));
					callCentreUtil.updateCaseHistory(StatusValues.ASSIGNED, Long.parseLong(map.get("caseId")), "Case is assigned for video conference", headers.get("UserName").toString().replace("[", "").replace("]", ""));
				}
				log.info(" VCSlots "+vcSlots);
				vcSlotsRepository.save(vcSlots);
				return new ResponseEntity<String>("Case is assigned for Video Conference", HttpStatus.OK);
			}catch (Exception e) {
				log.error("Problem encountered in callcenterservice at assignCaseToVc method. error: "+e.getMessage());
				return new ResponseEntity<String>("Problem encounter while assigning case to vc: "+e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}
	}
	
	public boolean checkTimeForOverLap(String startTime, String endTime, String scheduledStartTime, String scheduledEndTime){
		boolean isOverLap = false;
		try{
			Date time1 = new SimpleDateFormat("HH:mm:ss").parse(endTime);
		    Calendar endCalO = Calendar.getInstance();
		    endCalO.setTime(time1);
		    endCalO.add(Calendar.DATE, 1);
		    
		    Date time2 = new SimpleDateFormat("HH:mm:ss").parse(startTime);
		    Calendar startCalO = Calendar.getInstance();
		    startCalO.setTime(time2);
		    startCalO.add(Calendar.DATE, 1);
		     
		    Date d = new SimpleDateFormat("HH:mm:ss").parse(scheduledStartTime);
		    Calendar calendar3 = Calendar.getInstance();
		    calendar3.setTime(d);
		    calendar3.add(Calendar.DATE, 1);
		     
		    Date d2 = new SimpleDateFormat("HH:mm:ss").parse(scheduledEndTime);
		    Calendar calendar4 = Calendar.getInstance();
		    calendar4.setTime(d2);
		    calendar4.add(Calendar.DATE, 1);
		    
		    Date sTimeN = calendar3.getTime();
		    Date eTimeN = calendar4.getTime();
		    Date sTimeO = startCalO.getTime();
		    Date eTimeO = endCalO.getTime();
		    
		    if(sTimeN.compareTo(startCalO.getTime()) == 0 || sTimeN.compareTo(endCalO.getTime()) == 0 
		    		||  eTimeN.compareTo(startCalO.getTime()) == 0 || eTimeN.compareTo(endCalO.getTime()) == 0){
		    	isOverLap = true;
		    }else if ((sTimeN.after(startCalO.getTime()) && sTimeN.before(startCalO.getTime())) 
		    		|| (eTimeN.after(endCalO.getTime()) && eTimeN.before(endCalO.getTime()))) {
		        isOverLap = true;
		    }else if((sTimeN.after(sTimeO) && sTimeN.before(eTimeO))
		    		|| (eTimeN.after(sTimeO) && eTimeN.before(eTimeO))
		    		|| (sTimeO.after(sTimeN) && sTimeO.before(eTimeN))
		    		|| (eTimeO.after(sTimeN) && eTimeO.before(eTimeN))
		    		) {
		    	isOverLap = true;
		    }
		    
		}catch (Exception e) {
			e.printStackTrace();
		}
		log.info("logging from callcenterservice class, checkTimeForOverLap method"+isOverLap);
		return isOverLap;
	}

}
