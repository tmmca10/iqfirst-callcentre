package com.application.iqfirst.callcentre.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.application.iqfirst.callcentre.dto.CasePartnerMappingDTO;

@Service
public interface IQFirstCallCentreServices {
	
	public ResponseEntity<?> findAllCases(HttpHeaders headers, Map<String, String> map);
	
	public List<Map<Long,String>> getChilds(Long stateId);
	
	public List<String> getAllPinCodesOfCity(Long id);
	
	public ResponseEntity<?> findAllRelatedPartners(HttpHeaders headers, Map<String, String> map);
	
	public ResponseEntity<?> assignCaseToPartners(HttpHeaders headers, CasePartnerMappingDTO casePartnerMappingDTO);
	
	public ResponseEntity<?> updateCasePartnerMapping(HttpHeaders headers, CasePartnerMappingDTO casePartnerMappingDTO);
	
	public ResponseEntity<?> getAllCasePartnersMapping(HttpHeaders headers, Long mappingId);
	
	public ResponseEntity<?> updatePartnerStatus(HttpHeaders headers, Long partnerId, Character isActive);
	
	public ResponseEntity<?> assignCaseToVc(HttpHeaders headers, Map<String, String> map);
	
}
