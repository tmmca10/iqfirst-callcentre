package com.application.iqfirst.callcentre.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.application.iqfirst.callcentre.dto.CasePartnerMappingDTO;
import com.application.iqfirst.callcentre.service.IQFirstCallCentreServices;

@RestController
public class CallCentreController {
	
	private static final Logger log = LoggerFactory.getLogger(CallCentreController.class);
	
	@Autowired
	IQFirstCallCentreServices iQFirstCallCentreServices;
	
	
	/*This mapping is used to get all the case details along with filtered criteria, 
	 * if the POST body is null then it will fetch only un-assigned cases*/
	
	@RequestMapping(value = "/cases", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> getAllCaseDetails(@RequestHeader HttpHeaders headers, @RequestBody(required = false)  Map<String, String> map) {
		log.info("incoming map-values: "+map.toString());
		return iQFirstCallCentreServices.findAllCases(headers, map);
	}
	
	/*For the time being this methods are kept as it is, later it needs to be removed once confirmed that it wont required anymore*/
	
	@RequestMapping(value = "/childlocations", method = RequestMethod.GET, produces = "application/json")
	public List<Map<Long,String>> getAllChilds(@RequestParam Long sdcId){
		return iQFirstCallCentreServices.getChilds(sdcId);
	}
	
	/*For the time being this methods are kept as it is, later it needs to be removed once confirmed that it wont required anymore*/
	
	@RequestMapping(value = "/pincodes", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<String>> getpincodes(Long id){
		return ResponseEntity.ok(iQFirstCallCentreServices.getAllPinCodesOfCity(id));
	}
	
	/*This mapping is used to get all the Partners along with filtered criteria, 
	 * if the POST body is null then it will fetch all the available Partners from database
	 * To improve the search result it must have to be atleast one parameterized search along with stateId
	 * when stateId would be passed in body, it will fetch all the DC/ Partners within the state*/
	
	@RequestMapping(value = "/relatedpartners", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> getAllPartners(@RequestHeader HttpHeaders headers, @RequestBody(required = false)  Map<String, String> map){
		log.info("incoming map-values: "+map.toString());
		return iQFirstCallCentreServices.findAllRelatedPartners(headers, map);
	}
	
	/*This Mapping is used to get the list of assigned Case details with Partners/ DC*
	 * if Requestparam is provided then only specific assigned Case Details with Partners will be returned*/
	
	@RequestMapping(value = "/casetopartners", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllCasePartnersMapping(@RequestHeader HttpHeaders headers, @RequestParam(required=false) Long mappingId){
		log.info("mappingId: "+mappingId);
		return iQFirstCallCentreServices.getAllCasePartnersMapping(headers, mappingId);
	}
	
	/*This Mapping is used to Save the Case details with Partners/ DC
	 * As parameter it would accept casedetailsId, partnerId and scheduledDate
	 * once created, it will return the values of Case mapping details*/
	
	@RequestMapping(value = "/casetopartners", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> saveCasePartnerMapping(@RequestHeader HttpHeaders headers, @RequestBody CasePartnerMappingDTO casePartnerMappingDTO) {
		log.info("CasePartnerMappingDTO: "+casePartnerMappingDTO.toString());
		if(casePartnerMappingDTO.getMappingId() != null){
			return iQFirstCallCentreServices.updateCasePartnerMapping(headers, casePartnerMappingDTO);
		}else{
			return iQFirstCallCentreServices.assignCaseToPartners(headers, casePartnerMappingDTO);
		}
	}
	
	
	/*This API is used to Update Partners to active or de-active state*/
	@RequestMapping(value = "/partnerstatus", method = RequestMethod.POST)
	public ResponseEntity<?> updatePartnerStatus(@RequestHeader HttpHeaders headers, @RequestParam(required=false) Long partnerId, @RequestParam(required=false) char isActive ){
		log.info("partnerId: "+partnerId + "isActive :" +isActive);
		return iQFirstCallCentreServices.updatePartnerStatus(headers, partnerId, isActive);
	}
	
	@RequestMapping(value = "/casetovcuser", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> assignCaseToVcUsers(@RequestHeader HttpHeaders headers,
			@RequestBody(required = false)  Map<String, String> map){
		log.info("incoming map-values: "+map.toString());
		return iQFirstCallCentreServices.assignCaseToVc(headers, map);
	}

	
}
