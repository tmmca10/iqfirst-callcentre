package com.application.iqfirst.callcentre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallCentreApplication {
	public static void main(String[] args) {
		SpringApplication.run(CallCentreApplication.class, args);
	}
}
