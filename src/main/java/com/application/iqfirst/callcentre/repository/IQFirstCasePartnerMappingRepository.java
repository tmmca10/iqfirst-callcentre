package com.application.iqfirst.callcentre.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.application.iqfirst.callcentre.pojo.CasePartnerMapping;


public interface IQFirstCasePartnerMappingRepository extends JpaRepository<CasePartnerMapping, Long>{
	@Query("select cpm from CasePartnerMapping cpm where cpm.mappingId = :mappingId")
	public CasePartnerMapping findByID(@Param("mappingId") Long mappingId);
	
	@Query("select cpm from CasePartnerMapping cpm")
	public List<CasePartnerMapping> findAllCasePartnersMapping();
	
	@Query("select cpm from CasePartnerMapping cpm where cpm.casedetailsId = :casedetailsId")
	public List<CasePartnerMapping> findByCaseID(@Param("casedetailsId") Long casedetailsId);

}
