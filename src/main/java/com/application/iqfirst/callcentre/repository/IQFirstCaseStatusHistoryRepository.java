package com.application.iqfirst.callcentre.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.callcentre.pojo.CaseStatusHistory;

@Repository
public interface IQFirstCaseStatusHistoryRepository extends JpaRepository<CaseStatusHistory, Long>{

}
