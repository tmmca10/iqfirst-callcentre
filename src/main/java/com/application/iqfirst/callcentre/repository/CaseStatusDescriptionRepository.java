package com.application.iqfirst.callcentre.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.application.iqfirst.callcentre.pojo.CaseStatusDescription;


@Repository
public interface CaseStatusDescriptionRepository extends JpaRepository<CaseStatusDescription, Long> {
	@Query("select csd from CaseStatusDescription csd where csd.statusName = :statusName")
	public CaseStatusDescription findByName(@Param("statusName") String statusName);
	
	@Query("select csd from CaseStatusDescription csd where csd.statusId = :statusId")
	public CaseStatusDescription findByStatusId(@Param("statusId") int statusId);

}
