package com.application.iqfirst.callcentre.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.callcentre.pojo.VcSlots;

@Repository
public interface VcSlotsRepository extends JpaRepository<VcSlots, Long> {
	@Query("select vc from VcSlots vc where vc.conferenceDate = :conferenceDate and vc.assignedTo = :assignedTo")
	public List<VcSlots> findConferenceByDateTime(@Param("conferenceDate") Date conferenceDate, @Param("assignedTo") Long assignedTo);

}
