package com.application.iqfirst.callcentre.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.callcentre.pojo.CasePkgMapping;


@Repository
public interface CasePackageMappingRepository extends JpaRepository<CasePkgMapping, Long>{
	@Query("select cpm from CasePkgMapping cpm where cpm.caseId = :caseId")
	public List<CasePkgMapping> getpacakagesByCaseId(@Param("caseId") Long caseId);

}
