package com.application.iqfirst.callcentre.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.callcentre.pojo.CaseDetails;

@Repository
public interface IQFirstCaseDetailsRepository extends JpaRepository<CaseDetails, Long>, JpaSpecificationExecutor<CaseDetails> {
	@Query("select cd from CaseDetails cd where cd.caseId = :caseId")
	public CaseDetails findByID(@Param("caseId") Long caseId);
	
}
