package com.application.iqfirst.callcentre.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.callcentre.pojo.TestPackage;

@Repository
public interface IQFirstPackageRepository extends JpaRepository<TestPackage, Long>{
	
}
