package com.application.iqfirst.callcentre.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.callcentre.pojo.InsuranceCompany;

@Repository
public interface IQFirstInsuranceCompanyRepository extends JpaRepository<InsuranceCompany, Long> {
	@Query("select ic from InsuranceCompany ic where ic.compName = :CompName")
	public InsuranceCompany findByName(@Param("CompName") String CompName);
	
	@Query("select ic from InsuranceCompany ic where ic.compId = :compId")
	public InsuranceCompany findByComId(@Param("compId") Long compId);
	
}
