package com.application.iqfirst.callcentre.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.application.iqfirst.callcentre.pojo.PartnerDetail;

@Transactional
@RepositoryRestResource(collectionResourceRel = "partnerDtl", path = "partnerDtl")
public interface IQFirstPartnerDetailRepository extends JpaRepository<PartnerDetail, Long>,JpaSpecificationExecutor<PartnerDetail> {
	
	@Query("SELECT t.Pincode FROM PartnerDetail t where t.cityId = :cityId")
	List<Object[]> findPinCodes(@Param("cityId") Long cityId);
	
	@Query("SELECT t FROM PartnerDetail t where t.partnerId = :partnerId")
	PartnerDetail findByPartnerId(@Param("partnerId") Long partnerId);
}
