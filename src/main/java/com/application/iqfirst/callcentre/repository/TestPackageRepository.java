package com.application.iqfirst.callcentre.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.callcentre.pojo.TestPackage;

@Repository
public interface TestPackageRepository extends JpaRepository<TestPackage, Long> {
	@Query("select tp from TestPackage tp where tp.InsuranceCompany.compId = :compId and tp.packageCd= :packageCd")
	public List<TestPackage> findByComIdAndPackage(@Param("compId") Long compId, @Param("packageCd") String packageCd);

}
