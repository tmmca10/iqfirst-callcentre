package com.application.iqfirst.callcentre.repository;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.application.iqfirst.callcentre.pojo.GeoLocation;


@Transactional
@RepositoryRestResource(collectionResourceRel = "geolocations", path = "geoLocations")
public interface IQFirstGeoLocationRepository extends JpaRepository<GeoLocation, Long> {
	@Query("SELECT t FROM GeoLocation t where t.sdcId = :sdcId")
	GeoLocation findLocationById(@Param("sdcId") Long sdcId);
	
	@Query("SELECT t FROM GeoLocation t where t.parent.sdcId = :sdcId")
	List<GeoLocation> findChilds(@Param("sdcId") Long sdcId);
	
}
