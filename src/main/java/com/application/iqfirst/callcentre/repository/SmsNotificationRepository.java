package com.application.iqfirst.callcentre.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.callcentre.pojo.SmsNotificationView;

@Repository
public interface SmsNotificationRepository extends JpaRepository<SmsNotificationView, Long> {
	@Query("select snv from SmsNotificationView snv where snv.CaseId = :CaseId")
	SmsNotificationView getDetailsForNotification(@Param("CaseId") Long CaseId);

}
