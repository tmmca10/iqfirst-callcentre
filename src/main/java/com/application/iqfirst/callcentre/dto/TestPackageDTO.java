package com.application.iqfirst.callcentre.dto;

import com.application.iqfirst.callcentre.pojo.TestPackage;

public class TestPackageDTO {
	Long 	packageId;
	String 	packageCd;
	String 	packageTests;
	
	public TestPackageDTO(TestPackage testPackage){
		this.packageId 		= testPackage.getPackageId();
		this.packageCd 		= testPackage.getPackageCd();
		this.packageTests 	= testPackage.getPackageTests();
	}

	public Long getPackageId() {
		return packageId;
	}

	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	public String getPackageCd() {
		return packageCd;
	}

	public void setPackageCd(String packageCd) {
		this.packageCd = packageCd;
	}

	public String getPackageTests() {
		return packageTests;
	}

	public void setPackageTests(String packageTests) {
		this.packageTests = packageTests;
	}

	@Override
	public String toString() {
		return "TestPackageDTO [packageId=" + packageId + ", packageCd=" + packageCd + ", packageTests=" + packageTests
				+ "]";
	}
	
}
