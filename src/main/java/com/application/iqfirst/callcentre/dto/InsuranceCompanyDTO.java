package com.application.iqfirst.callcentre.dto;

public class InsuranceCompanyDTO {
	Long 	compId;
	String 	compName;
	
	public InsuranceCompanyDTO(){}
	
	public InsuranceCompanyDTO(Long compId, String compName) {
		this.compId 	= compId;
		this.compName 	= compName;
	}

	public Long getCompId() {
		return compId;
	}

	public void setCompId(Long compId) {
		this.compId = compId;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	@Override
	public String toString() {
		return "InsuranceCompanyDTO [compId=" + compId + ", compName=" + compName + "]";
	}
	
}
