package com.application.iqfirst.callcentre.dto;

import java.util.Set;


public class CaseDetailsDTO {
	String			caseId;
	String			customerCode;
	String 			insuranceCompany;
	String 			policyNo;
	String 			customerName;
	String 			address;
	String 			pin;
	String 			latLng;
	String 			regDt;
	String 			customerCO;
	String			dist;
	String 			city;
	String 			state;
	Long			distId;
	Long 			cityId;
	Long 			stateId;
	String 			mobileNo1;
	String 			mobileNo2;
	String 			sa;
	String 			caseType;
	String 			paymentBy;
	String			billId;
	String 			partnerBillId;
	String 			caseStatus;
	String 			createdBy;
	String 			createddate;
	String 			updatedby;
	String 			updateddate;
	Set<String> 	packages;
	String 			isVideoUser;
	
	public CaseDetailsDTO(){}
	

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getLatLng() {
		return latLng;
	}

	public void setLatLng(String latLng) {
		this.latLng = latLng;
	}

	public String getRegDt() {
		return regDt;
	}

	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

	public String getCustomerCO() {
		return customerCO;
	}

	public void setCustomerCO(String customerCO) {
		this.customerCO = customerCO;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long getDistId() {
		return distId;
	}

	public void setDistId(Long distId) {
		this.distId = distId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public String getMobileNo1() {
		return mobileNo1;
	}

	public void setMobileNo1(String mobileNo1) {
		this.mobileNo1 = mobileNo1;
	}

	public String getMobileNo2() {
		return mobileNo2;
	}

	public void setMobileNo2(String mobileNo2) {
		this.mobileNo2 = mobileNo2;
	}

	public String getSa() {
		return sa;
	}

	public void setSa(String sa) {
		this.sa = sa;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getPaymentBy() {
		return paymentBy;
	}

	public void setPaymentBy(String paymentBy) {
		this.paymentBy = paymentBy;
	}
	
	public Set<String> getPackages() {
		return packages;
	}

	public void setPackages(Set<String> packages) {
		this.packages = packages;
	}
	
	public String getDist() {
		return dist;
	}

	public void setDist(String dist) {
		this.dist = dist;
	}

	public String getBillId() {
		return billId;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public String getPartnerBillId() {
		return partnerBillId;
	}

	public void setPartnerBillId(String partnerBillId) {
		this.partnerBillId = partnerBillId;
	}

	public String getCaseStatus() {
		return caseStatus;
	}

	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreateddate() {
		return createddate;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public String getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(String updateddate) {
		this.updateddate = updateddate;
	}

	public String getIsVideoUser() {
		return isVideoUser;
	}

	public void setIsVideoUser(String isVideoUser) {
		this.isVideoUser = isVideoUser;
	}

	@Override
	public String toString() {
		return "CaseDetailsDTO [caseId=" + caseId + ", customerCode=" + customerCode + ", insuranceCompany="
				+ insuranceCompany + ", policyNo=" + policyNo + ", customerName=" + customerName + ", address="
				+ address + ", pin=" + pin + ", latLng=" + latLng + ", regDt=" + regDt + ", customerCO=" + customerCO
				+ ", dist=" + dist + ", city=" + city + ", state=" + state + ", distId=" + distId + ", cityId=" + cityId
				+ ", stateId=" + stateId + ", mobileNo1=" + mobileNo1 + ", mobileNo2=" + mobileNo2 + ", sa=" + sa
				+ ", caseType=" + caseType + ", paymentBy=" + paymentBy + ", billId=" + billId + ", partnerBillId="
				+ partnerBillId + ", caseStatus=" + caseStatus + ", createdBy=" + createdBy + ", createddate="
				+ createddate + ", updatedby=" + updatedby + ", updateddate=" + updateddate + ", packages=" + packages
				+ ", isVideoUser=" + isVideoUser + "]";
	}
	
}
