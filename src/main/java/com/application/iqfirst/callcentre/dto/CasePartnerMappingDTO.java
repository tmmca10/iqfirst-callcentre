package com.application.iqfirst.callcentre.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;


@SuppressWarnings("serial")
public class CasePartnerMappingDTO implements Serializable{
	private Long mappingId;
	private Long casedetailsId;
	private Long partnerId;
	private String customerName;
	private String customerCode;
	private String customerState;
	private String customerDistrict;
	private String customerCity;
	private String customerMobileNo;
	private String policyNumber;
	private Set<String> packages;
	private String providerName;
	private String providerCode;
	private String scheduledDate;
	private Character isHomeVisit;
	private Timestamp createdDate;
	private String createdBy;
	private Timestamp modifiedDate;
	private String modifiedBy;
	
	public CasePartnerMappingDTO(Long mappingId, Long casedetailsId, Long partnerId, String customerName,
			String customerCode,String customerState, String customerDistrict, String customerCity, String customerMobileNo,
			String policyNumber, Set<String> packages, String providerName, String providerCode, String scheduledDate, Character isHomeVisit,
			Timestamp createdDate, String createdBy, Timestamp modifiedDate, String modifiedBy) {
		this.mappingId = mappingId;
		this.casedetailsId = casedetailsId;
		this.partnerId = partnerId;
		this.customerName = customerName;
		this.customerState = customerState;
		this.customerDistrict = customerDistrict;
		this.customerCity = customerCity;
		this.customerMobileNo = customerMobileNo;
		this.policyNumber = policyNumber;
		this.packages = packages;
		this.providerName = providerName;
		this.providerCode = providerCode;
		this.scheduledDate = scheduledDate;
		this.isHomeVisit = isHomeVisit;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
		this.customerCode = customerCode;
	}

	public CasePartnerMappingDTO() {}
	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public String getCustomerDistrict() {
		return customerDistrict;
	}

	public void setCustomerDistrict(String customerDistrict) {
		this.customerDistrict = customerDistrict;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerMobileNo() {
		return customerMobileNo;
	}

	public void setCustomerMobileNo(String customerMobileNo) {
		this.customerMobileNo = customerMobileNo;
	}

	public Set<String> getPackages() {
		return packages;
	}

	public void setPackages(Set<String> packages) {
		this.packages = packages;
	}

	public Long getMappingId() {
		return mappingId;
	}

	public void setMappingId(Long mappingId) {
		this.mappingId = mappingId;
	}

	public Long getCasedetailsId() {
		return casedetailsId;
	}

	public void setCasedetailsId(Long casedetailsId) {
		this.casedetailsId = casedetailsId;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(String scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public Character getIsHomeVisit() {
		return isHomeVisit;
	}

	public void setIsHomeVisit(Character isHomeVisit) {
		this.isHomeVisit = isHomeVisit;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	@Override
	public String toString() {
		return "CasePartnerMappingDTO [mappingId=" + mappingId + ", casedetailsId=" + casedetailsId + ", partnerId="
				+ partnerId + ", customerName=" + customerName + ", customerCode=" + customerCode + ", customerState="
				+ customerState + ", customerDistrict=" + customerDistrict + ", customerCity=" + customerCity
				+ ", customerMobileNo=" + customerMobileNo + ", policyNumber=" + policyNumber + ", packages=" + packages
				+ ", providerName=" + providerName + ", providerCode=" + providerCode + ", scheduledDate="
				+ scheduledDate + ", isHomeVisit=" + isHomeVisit + ", createdDate=" + createdDate + ", createdBy="
				+ createdBy + ", modifiedDate=" + modifiedDate + ", modifiedBy=" + modifiedBy + "]";
	}
	
}
