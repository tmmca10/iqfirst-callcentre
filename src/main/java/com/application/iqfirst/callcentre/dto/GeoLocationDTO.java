package com.application.iqfirst.callcentre.dto;

public class GeoLocationDTO {
	
	private String locationName;
	
	private String parentId;

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

}
